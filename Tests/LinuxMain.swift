import XCTest

import FDCReaderTests

var tests = [XCTestCaseEntry]()
tests += FDCReaderTests.allTests()
XCTMain(tests)
